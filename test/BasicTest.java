import org.junit.*;
import java.util.*;
import play.test.*;
import models.*;

public class BasicTest extends UnitTest {

	User user;
	Campaign campaign;
	Artist bieber;
	Artist daftpunk;
	City athens;
	City thess;

	@Test
	// Testing the USer
	public void aVeryImportantThingToTest() {

		// Guy gets to gigalize and creates a profile.Let's create 2 users
		user = new User("Nasos A.", "tha.anagno@gmail.com", "1234").save();
		user = new User("David B.", "david@gmail.com", "1234").save();

		// Do they exist in the db?Fuck yeah
		assertEquals(User.findAll().size(), 2);

		// Do the queries work?
		User nasosUser = User.find("byMail", "a@a.com").first();.first();
		assertEquals(nasosUser.getPass(), "1234");

		// So we have some artists
		bieber = new Artist("Justin Bieber", "crap").save();
		daftpunk = new Artist("Daft punk", "electronic").save();

		// Do some db testing
		assertEquals(Artist.findAll().size(), 2);
		Artist isHeBieber = Artist.find("byName", "Justin Bieber").first();
		assertEquals(isHeBieber.getGenre(), "crap");

		// Create citys
		athens = new City("Athens", 131231, 23423).save();
		thess = new City("Thess", 123123, 123123).save();

		// do some testing with the cities
		assertEquals(City.findAll().size(), 2);
		City isItAthensFFS = City.find("byName", "Athens").first();
		assertEquals(isItAthensFFS.getLang(), 131231);

		// The user creates 3 campaigns
		// Two for bieber ( commit suicide )
		campaign = new Campaign(nasosUser, bieber, athens, 3000).save();
		nasosUser.addCampaign(campaign);
		campaign = new Campaign(nasosUser, bieber, thess, 3000).save();
		nasosUser.addCampaign(campaign);
		// One for daftpunk
		campaign = new Campaign(nasosUser, daftpunk, athens, 3000).save();
		nasosUser.addCampaign(campaign);

		// Testing for database
		assertEquals(Campaign.findAll().size(), 3);
		// Ok nasos how many campaigns do you have ,biatch
		List<Campaign> campaignsOfThisCrazyGuyBoom = nasosUser.campaigns;
		// So show us what you've got
		for (Campaign c : campaignsOfThisCrazyGuyBoom) {
			System.out.println(c.getArtist().getName()+ " in " + c.getCity().getName() + " with  " + c.getGoal() + " backer ");
			
		}

	}

}
