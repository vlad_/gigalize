package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import play.db.jpa.Model;

@Entity
public class Artist extends Model{

	//Name of the artis
	private String name;
	//Genre of music
	private String genre;
	//Campaigns started for the specific artist
	@OneToMany(mappedBy = "artist", cascade = CascadeType.ALL)
	public List<Campaign> campaigns;
	
	public Artist(String name,String genre){
		
		setName(name);
		setGenre(genre);
		campaigns = new ArrayList();
	
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
}
