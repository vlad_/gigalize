package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import play.db.jpa.Model;
import play.libs.Codec;

/**
 * 
 * @author Nasos A. This is the class for the simple User.Will add Facebook
 * 
 */
@Entity
public class User extends Model {

	// The name of the user
	private String name;

	// The mail of the user
	private String mail;

	// The pass of the user
	private String pass;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	// The campaigns of the user
	public List<Campaign> campaigns;

	public User(String name, String mail, String pass) {
		setName(name);
		setMail(mail);
		setPass(pass);
		// initialize
		campaigns = new ArrayList();
	}

	public User addCampaign(Campaign campaign) {
		this.campaigns.add(campaign);
		return this.save();
	}

	// Method to check the validity of the credentials
	public static boolean isValidLogin(String mail, String pass) {
		// return TRUE if there is a single matching username/passwordHash

		return (count("mail=? AND pass=?", mail, pass) == 1);

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

}
