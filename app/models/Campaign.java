package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import play.db.jpa.Model;

@Entity
public class Campaign extends Model {

	// Campaign belongs to user
	@ManyToOne
	public User user;

	@ManyToOne
	// Campaign has to do with an Artist
	private Artist artist;
	@OneToOne
	// City that the gig is gonna take place
	private City city;
	// Gigalized goal
	private int goal;
	// Number of the backers
	private int backers;

	public Campaign(User user, Artist artist, City city, int goal) {
		this.user = user;
		setArtist(artist);
		setCity(city);
		setGoal(goal);
		setBacker(0);
	}
	
	public void addBackers(){
		this.backers++;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public int getGoal() {
		return goal;
	}

	public void setGoal(int goal) {
		this.goal = goal;
	}

	public int getBacker() {
		return backers;
	}

	public void setBacker(int backers) {
		this.backers = backers;
	}
}
