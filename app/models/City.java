package models;

import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity
public class City extends Model {

	private String name;
	private int lang;
	private int longt;

	
	public City(String name,int lang,int longt){
		
		setName(name);
		setLang(lang);
		setLongt(longt);
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLang() {
		return lang;
	}

	public void setLang(int lang) {
		this.lang = lang;
	}

	public int getLongt() {
		return longt;
	}

	public void setLongt(int longt) {
		this.longt = longt;
	}

}
