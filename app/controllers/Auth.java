package controllers;

import com.google.gson.JsonObject;

import models.User;
import models.User;
import play.data.validation.Email;
import play.data.validation.Required;
import play.libs.Codec;
import play.mvc.Controller;

/**
 * Login,Logout pretty much the security things
 * 
 * @author deauth
 * 
 */
public class Auth extends Controller {

	/**
	 * DUDE CREATE USER
	 * 
	 * @param name
	 * @param mail
	 * @param pass
	 */
	// this is going to be used to parse and verify
	// the form supplied from the user(POST)
	public static void createUser(@Required String name,
			@Required @Email String mail, @Required String pass) {
		// if we have errors

		if (validation.hasErrors()) {
			params.flash();
			validation.keep();
			register();
			// checks if the emailAd already exists
		} else if (User.find("byMail", mail).fetch().size() != 0) {
			System.out.println("already exists");
			validation.addError("mail", "The email address already exists");
			validation.keep();
			register();

		} else {

			// if we don't have any
			User User = new User(name, mail, pass).save();
			Application.index();

		}

	}

	public static void register() {
		render();
	}

	/**
	 * Check if email exists
	 */
	public static void checkMail(String mail) {
		String msg;
		if (User.find("byMail", mail).fetch().isEmpty()) {
			
			System.out.println("not");
			msg = "{\"status\":false}";
		} else {
			
			System.out.println("exists");
			msg = "{\"status\":true}";
		}
		renderJSON(msg);
	}

	/**
	 * Log the user in
	 * 
	 * @param mail
	 * @param id
	 */
	private static void doLoginLogic(String mail, long id) {
		System.out.println(Codec.hexSHA1(mail + id));
		session.put("user", mail);
		session.put("id", id);
		session.put(
				"smart",
				Codec.hexSHA1(Codec.encodeBASE64(mail)
						+ Codec.encodeBASE64(String.valueOf(id))));
	}

	/**
	 * Method that will assign the pseudo session to the user
	 * 
	 * @param mail
	 * @param pass
	 */
	public static void doLogin(String mail, String pass) {
		if (User.isValidLogin(mail, pass) == true) {
			// If user has an account and he IS verified
			System.out.println("user logged in");
			User user = User.find("byMail", mail).first();
			doLoginLogic(mail, user.getId());
			Application.profile();
			// If user is NOT verified throw error

		} else {
			System.out.println("mail:"+mail+" and pass:"+pass+"have error");
			validation.addError("username",
					"Username/Password combination is not valid");
			validation.keep();
			Auth.login();

		}
	}

	/**
	 * Page to log in
	 */
	public static void login() {
		if (session.get("user") == null) {
			render();
		} else {
			Application.profile();
		}

	}

	/**
	 * Clear the session
	 */
	public static void logout() {
		session.clear();
		Application.index();
	}

	/**
	 * Check the creds
	 * 
	 * @param emailAd
	 * @param id
	 * @param smart
	 * @return
	 */
	private static boolean checkCredentials(String emailAd, String id,
			String smart) {
		String checksum = Codec.hexSHA1(Codec.decodeBASE64(emailAd).toString()
				+ Codec.decodeBASE64(id).toString());
		if (checksum == smart) {
			return true;
		}
		return false;
	}

}
