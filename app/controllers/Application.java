package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Application extends Controller {

	// Make a random marketing page
	public static void index() {
		render();
	}

	// Make a page that will list all the gigs
	public static void listGigs() {
		// Find all the gigs and return them
		List<Campaign> allCampaigns = Campaign.findAll();
		render(allCampaigns);
	}

	/**
	 * Dude give me the form for the campaign creation
	 */
	public static void formCampaign() {
		if (session.get("user") != null) {
			render();
		} else {
			Auth.login();
		}
	}

	/**
	 * So take the name of the artist and the name of the city with the goal
	 * from the form and create the campaign
	 * 
	 * @param artistName
	 * @param cityName
	 * @param goal
	 */
	public static void doCreateCampaign(String artistName, String cityName,
			int goal) {
		if (session.get("user") != null) {

			Artist artist = Artist.find("byName", artistName).first();
			City city = City.find("byName", cityName).first();
			User user = User.find("byMail", session.get("user")).first();
			Campaign newCampaign = new Campaign(user, artist, city, goal)
					.save();
			user.addCampaign(newCampaign);
			
		} else {
			Auth.login();
		}
	}

	// Make a page for user's profile
	public static void profile() {
		render();
	}

	// Make a page for seperate campaign
	public static void viewCampaign(long id) {
		Campaign campaign = Campaign.findById(id);
		render(campaign);
	}

}